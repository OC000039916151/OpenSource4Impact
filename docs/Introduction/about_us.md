

<div class="grid cards team" markdown>

-   ![Dr. Linda Kleemann Github Profile](https://avatars.githubusercontent.com/u/161041924?v=4)

    #### Dr. Linda Kleemann

    [*GFA Consoluting Group GmbH*](https://www.gfa-group.de/)

    :material-email: [linda.kleemann@gfa-group.de](mailto:linda.kleemann@gfa-group.de)
    
    :material-github: [gelicko](https://github.com/gelicko)

-   ![Dr. Fred Jendrzejewski Github Profile](https://avatars.githubusercontent.com/u/8323674?v=4)

    #### Dr. Fred Jendrzejewski

    [*KfW*](https://www.gfa-group.de/)
    
    :material-email: [fred.jendrzejewski@kfw.de](mailto:fred.jendrzejewski@kfw.de)

    :material-github: [fretchen](https://github.com/fretchen)

-   ![Philipp Schreiber Github Profile](https://avatars.githubusercontent.com/u/82368796?v=4)

    #### Philipp Schreiber

    [*GFA Consoluting Group GmbH*](https://www.gfa-group.de/)
    
    :material-email: [philipp.schreiber@gfa-group.de](mailto:philipp.schreiber@gfa-group.de)

    :material-github: [pcschreiber1](https://github.com/pcschreiber1)
</div>